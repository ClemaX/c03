/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlcat.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/06 21:58:01 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/07 15:09:23 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	const char	*start = src;
	int			len_dest;

	len_dest = 0;
	while (*dest)
	{
		dest++;
		len_dest++;
	}
	if (size == 0)
	{
		while (*src)
			src++;
	}
	while (*src && src - start + 1 < size)
	{
		*dest = *src;
		src++;
		dest++;
	}
	*dest = '\0';
	return (len_dest + (src - start));
}
