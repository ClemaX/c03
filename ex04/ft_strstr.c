/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strstr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/05 20:55:51 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/07 15:17:41 by chamada     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

static int		ft_strlen(char *str)
{
	int len;

	len = 0;
	while (*str != '\0')
	{
		len++;
		str++;
	}
	return (len);
}

static int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	char *start;

	if (n == 0)
		return (0);
	start = s1;
	while ((*s1 != '\0' && *s2 != '\0') && *s1 == *s2 && s1 - start + 1 < n)
	{
		s1++;
		s2++;
	}
	if (*s1 == *s2)
		return (0);
	return (*s1 - *s2);
}

char			*ft_strstr(char *str, char *to_find)
{
	int to_find_len;

	to_find_len = ft_strlen(to_find);
	if (to_find_len == 0)
		return (str);
	while (*str)
	{
		while (*str != *to_find)
			str++;
		if (ft_strncmp(str, to_find, to_find_len) == 0)
			return (str);
	}
	return (0);
}
